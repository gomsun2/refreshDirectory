unit v.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RzCommon, RzButton, System.ImageList, Vcl.ImgList, RzStatus,
  Vcl.ExtCtrls, RzPanel;

type
  TvMain = class(TForm)
    Reg: TRzRegIniFile;
    GroupBox1: TGroupBox;
    ImageList1: TImageList;
    ButtonAdd: TRzBitBtn;
    ButtonRemove: TRzBitBtn;
    ButtonExecute: TRzBitBtn;
    RzVersionInfo1: TRzVersionInfo;
    procedure FormCreate(Sender: TObject);

    procedure ButtonExecuteClickClick(Sender: TObject);
    procedure ButtonAddClick(Sender: TObject);
    procedure ButtonRemoveClick(Sender: TObject);
  private
    FRegValue: String;
    procedure RefreshReg;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  ShlObj, System.IOUtils
  ;

const
  SShellRoot = 'Software\Classes\directory\Background\shell';
  SAppRoot = SShellRoot + '\Reset View';
  SSec = 'Command';

procedure TvMain.ButtonAddClick(Sender: TObject);
begin
  Reg.Path := SShellRoot;
  Reg.EraseSection('Reset View');

  Reg.Path := SAppRoot;
  Reg.WriteString(SSec, '', FRegValue);

  RefreshReg;
end;

procedure TvMain.ButtonExecuteClickClick(Sender: TObject);
begin
  SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, nil, nil);
end;

procedure TvMain.ButtonRemoveClick(Sender: TObject);
begin
  Reg.Path := SShellRoot;
  Reg.EraseSection('Reset View');

  RefreshReg;
end;

procedure TvMain.RefreshReg;
begin
  ButtonAdd.Enabled := not Reg.SectionExists(SSec);
  ButtonRemove.Enabled := not ButtonAdd.Enabled;
end;

procedure TvMain.FormCreate(Sender: TObject);
begin
  Caption := Format(Caption, [RzVersionInfo1.FileVersion]);

  FRegValue := ParamStr(0) + ' "r"';

  Reg.Path := SAppRoot;
  RefreshReg;
end;

end.
