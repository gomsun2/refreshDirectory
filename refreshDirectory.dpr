program refreshDirectory;

uses
  ShlObj,

  Vcl.Forms,
  v.main in 'v.main.pas' {vMain};

{$R *.res}

begin
  if ParamCount > 0 then
    if ParamStr(1) = 'r' then
    begin
      SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, nil, nil);
      Exit;
    end;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TvMain, vMain);
  Application.Run;
end.
